.PHONY: test
test:
	go test -race -coverprofile=coverage.out -tags test

.PHONY: cover
cover: 
	go test -race -coverprofile=coverage.out -covermode=atomic -tags test

.PHONY: coverhtml
coverhtml: 
	go tool cover -html=coverage.out

# make buildrun input=./tests/input.txt
.PHONY: buildrun
buildrun: 
	go build && ./intern-tech-challenge $(input)