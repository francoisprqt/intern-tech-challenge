//+build !test

package main

import (
	"context"
	"fmt"
	"os"

	"github.com/google/go-github/github"
)

// Here we implement the basics of communicating with github through the library as well as printing the version
// You will need to implement LatestVersions function as well as make this application support the file format outlined in the README
// Please use the format defined by the fmt.Printf line at the bottom, as we will define a passing coding challenge as one that outputs
// the correct information, including this line
func main() {
	// first check and read file
	filePath := os.Args[1]
	f, err := os.Open(filePath)
	defer f.Close()
	if err != nil {
		panic(err)
	}
	// Github
	client := &githubClient{github.NewClient(nil)}
	ctx := context.Background()
	result, err := parseFile(ctx, client, f)
	if err != nil {
		fmt.Println("Done with error")
		fmt.Println(err.Error())
	}
	fmt.Println("Result:")
	if result != nil {
		for _, r := range result {
			fmt.Println(r)
		}
	}
}
