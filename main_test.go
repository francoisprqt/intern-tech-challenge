package main

import (
	"context"
	"errors"
	"io"
	"strings"
	"testing"

	"github.com/coreos/go-semver/semver"
	"github.com/google/go-github/github"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func stringToVersionSlice(stringSlice []string) []*semver.Version {
	versionSlice := make([]*semver.Version, len(stringSlice))
	for i, versionString := range stringSlice {
		versionSlice[i] = semver.New(versionString)
	}
	return versionSlice
}

func versionToStringSlice(versionSlice []*semver.Version) []string {
	stringSlice := make([]string, len(versionSlice))
	for i, version := range versionSlice {
		stringSlice[i] = version.String()
	}
	return stringSlice
}

func TestLatestVersions(t *testing.T) {
	testCases := []struct {
		versionSlice   []string
		expectedResult []string
		minVersion     *semver.Version
	}{
		{
			versionSlice:   []string{"1.8.11", "1.9.6", "1.10.1", "1.9.5", "1.8.10", "1.10.0", "1.7.14", "1.8.9", "1.9.5"},
			expectedResult: []string{"1.10.1", "1.9.6", "1.8.11"},
			minVersion:     semver.New("1.8.0"),
		},
		{
			versionSlice:   []string{"1.8.11", "1.9.6", "1.10.1", "1.9.5", "1.8.10", "1.10.0", "1.7.14", "1.8.9", "1.9.5"},
			expectedResult: []string{"1.10.1", "1.9.6"},
			minVersion:     semver.New("1.8.12"),
		},
		{
			versionSlice:   []string{"1.10.1", "1.9.5", "1.8.10", "1.10.0", "1.7.14", "1.8.9", "1.9.5"},
			expectedResult: []string{"1.10.1"},
			minVersion:     semver.New("1.10.0"),
		},
		{
			versionSlice:   []string{"2.2.1", "2.2.0"},
			expectedResult: []string{"2.2.1"},
			minVersion:     semver.New("2.2.1"),
		},
		{
			versionSlice:   []string{"2.2.1", "2.2.0", "0.34.5"},
			expectedResult: []string{"2.2.1"},
			minVersion:     semver.New("2.2.1"),
		},
		{
			versionSlice:   []string{"2.2.0", "0.34.5", "2.2.1", "4.54.12", "1.21.4"},
			expectedResult: []string{"4.54.12", "2.2.1", "1.21.4"},
			minVersion:     semver.New("1.2.1"),
		},
		{
			versionSlice:   []string{"2.2.0", "0.34.5", "0.34.6", "0.34.12", "2.2.1", "4.54.12", "1.21.4"},
			expectedResult: []string{"4.54.12", "2.2.1", "1.21.4", "0.34.12"},
			minVersion:     semver.New("0.2.1"),
		},
	}

	test := func(versionData []string, expectedResult []string, minVersion *semver.Version) {
		stringSlice := versionToStringSlice(LatestVersions(stringToVersionSlice(versionData), minVersion))
		for i, versionString := range stringSlice {
			if versionString != expectedResult[i] {
				t.Errorf("Received %s, expected %s", stringSlice, expectedResult)
				return
			}
		}
	}

	for _, testValues := range testCases {
		test(testValues.versionSlice, testValues.expectedResult, testValues.minVersion)
	}
}

type testRepositoriesService struct {
	mock.Mock
	releases []*github.RepositoryRelease
	res      *github.Response
	err      error
}

func (t *testRepositoriesService) ListReleases(ctx context.Context, author, repo string, opt *github.ListOptions) ([]*github.RepositoryRelease, *github.Response, error) {
	return t.releases, t.res, t.err
}

type testGithubClient struct {
	mock.Mock
	repoService RepositoriesService
}

func (t *testGithubClient) Repositories() RepositoriesService {
	return t.repoService
}

func newRelease(tag string) *github.RepositoryRelease {
	return &github.RepositoryRelease{
		TagName: &tag,
	}
}

func assertUnorderedStringSlice(t *testing.T, expected []string, result []string) {
	for _, s := range expected {
		var found bool
		for _, ss := range result {
			if found = s == ss; found {
				break
			}
		}
		assert.True(t, found, "string should be present")
	}
}

func TestGithubAPIIntegration(t *testing.T) {
	testCases := []struct {
		test         string
		reader       io.Reader
		client       GithubClient
		expectations func(*testing.T, []string, error)
	}{
		{
			test: "basic",
			reader: strings.NewReader(
				"repository,min_version\n" +
					"hello/world,0.1.0\n" +
					"foo/bar,1.2.5",
			),
			client: &testGithubClient{
				repoService: &testRepositoriesService{
					res: &github.Response{NextPage: 0},
					releases: []*github.RepositoryRelease{
						newRelease("0.1.1"),
						newRelease("1.0.1"),
						newRelease("1.1.3"),
						newRelease("1.0.4-rc-alpha"),
						newRelease("2.6.1"),
						newRelease("2.6.10"),
					},
				},
			},
			expectations: func(t *testing.T, result []string, err error) {
				assertUnorderedStringSlice(t, []string{
					"foo/bar: [2.6.10]",
					"hello/world: [2.6.10 1.1.3 1.0.1 0.1.1]",
				}, result)
				assert.Nil(t, err)
			},
		},
		{
			test: "basic2",
			reader: strings.NewReader(
				"repository,min_version\n" +
					"hello/world,0.1.0\n" +
					"foo/bar,1.2.5",
			),
			client: &testGithubClient{
				repoService: &testRepositoriesService{
					releases: []*github.RepositoryRelease{
						newRelease("0.1.1"),
						newRelease("1.0.1"),
						newRelease("v1.1.3"),
						newRelease("1.0.4-rc-alpha"),
						newRelease("2.6.1"),
						newRelease("2.6.10"),
					},
				},
			},
			expectations: func(t *testing.T, result []string, err error) {
				assertUnorderedStringSlice(t, []string{
					"foo/bar: [2.6.10]",
					"hello/world: [2.6.10 1.1.3 1.0.1 0.1.1]",
				}, result)
				assert.Nil(t, err)
			},
		},
		{
			test: "basic-error",
			reader: strings.NewReader(
				"repository,min_version\n" +
					"hello/world,0.1.0\n" +
					"foo/bar,1.2.5",
			),
			client: &testGithubClient{
				repoService: &testRepositoriesService{
					err: errors.New("test error"),
				},
			},
			expectations: func(t *testing.T, result []string, err error) {
				assert.Equal(t, result, []string{})
				assert.NotNil(t, err)
			},
		},
		{
			test: "invalid-repo-error",
			reader: strings.NewReader(
				"repository,min_version\n" +
					"helloworld,0.1.0\n" +
					"foo/bar,1.2.5",
			),
			client: &testGithubClient{
				repoService: &testRepositoriesService{
					err: errors.New("test error"),
				},
			},
			expectations: func(t *testing.T, result []string, err error) {
				assert.Equal(t, []string{}, result)
				assert.NotNil(t, err)
				assert.Equal(t, "Repo malformed, entry provided 'helloworld,0.1.0'", err.Error())
			},
		},
		{
			test: "invalid-repo-error2",
			reader: strings.NewReader(
				"repository,min_version\n" +
					"helloworld0.1.0\n" +
					"foo/bar,1.2.5",
			),
			client: &testGithubClient{
				repoService: &testRepositoriesService{
					err: errors.New("test error"),
				},
			},
			expectations: func(t *testing.T, result []string, err error) {
				assert.Equal(t, []string{}, result)
				assert.NotNil(t, err)
				assert.Equal(t, "Repo malformed, entry provided 'helloworld0.1.0'", err.Error())
			},
		},
		{
			test: "invalid-repo-error2",
			reader: strings.NewReader(
				"repository,min_version\n" +
					"hello/world,0.é.0\n" +
					"foo/bar,1.2.5",
			),
			client: &testGithubClient{
				repoService: &testRepositoriesService{
					err: errors.New("test error"),
				},
			},
			expectations: func(t *testing.T, result []string, err error) {
				assert.Equal(t, []string{}, result)
				assert.NotNil(t, err)
				assert.Equal(t, "Err while parsing version for repo 'hello/world': strconv.ParseInt: parsing \"é\": invalid syntax", err.Error())
			},
		},
		{
			test: "no-repo",
			reader: strings.NewReader(
				"repository,min_version\n",
			),
			client: &testGithubClient{
				repoService: &testRepositoriesService{
					err: errors.New("test error"),
				},
			},
			expectations: func(t *testing.T, result []string, err error) {
				assert.Equal(t, []string{}, result)
				assert.Nil(t, err)
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.test, func(t *testing.T) {
			result, err := parseFile(context.Background(), testCase.client, testCase.reader)
			t.Log(result)
			testCase.expectations(t, result, err)
		})
	}
}
