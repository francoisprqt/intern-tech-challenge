package main

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"io"
	"sort"
	"strings"

	"github.com/coreos/go-semver/semver"
	"github.com/google/go-github/github"
)

type repoVersions struct {
	repo     string
	versions []*semver.Version
}

// RepositoriesService is an interface to make github client mockable
type RepositoriesService interface {
	ListReleases(context.Context, string, string, *github.ListOptions) ([]*github.RepositoryRelease, *github.Response, error)
}

// GithubClient is an interface to make *github.Client mockable
type GithubClient interface {
	Repositories() RepositoriesService
}
type githubClient struct {
	client *github.Client
}

func (g *githubClient) Repositories() RepositoriesService {
	return g.client.Repositories
}

// String implements Stringer
func (r *repoVersions) String() string {
	versionString := "["
	last := len(r.versions) - 1
	for i, v := range r.versions {
		if i == last {
			versionString += v.String()
			continue
		}
		versionString += v.String() + " "
	}
	versionString += "]"
	return r.repo + ": " + versionString
}

// LatestVersions returns a sorted slice with the highest version as its first element and the highest version of the smaller minor versions in a descending order
func LatestVersions(releases []*semver.Version, minVersion *semver.Version) []*semver.Version {
	// if no releases just return right away
	releasesLen := len(releases)
	if releasesLen == 0 {
		return nil
	}
	sort.Sort(sort.Reverse(semver.Versions(releases)))
	// create our buffered slice
	var versionSlice = make([]*semver.Version, 0)
	var ver *semver.Version
	// This is just an example structure of the code, if you implement this interface, the test cases in main_test.go are very easy to run
	for i, release := range releases {
		if minVersion.LessThan(*release) {
			// If we're looking at the first version in the list and it's higher than our minVersion, it must be a valid result
			if i == 0 {
				ver = release
				versionSlice = append(versionSlice, release)
			}
			// If the major/minor has changed it must be a valid result
			if !(ver.Major == release.Major && ver.Minor == release.Minor) {
				versionSlice = append(versionSlice, release)
			}
			ver = release
		}
	}
	return versionSlice
}

func sanitizeVersionString(ver string) (string, error) {
	if ver[0] == 'v' {
		ver = ver[1:]
	}
	if strings.Contains(ver, "alpha") || strings.Contains(ver, "beta") {
		return "", errors.New("Version is a pre-release")
	}
	return ver, nil
}

func getFollowedVersions(ctx context.Context, repoService RepositoriesService, repo []string, minVer *semver.Version, resultChan chan<- *repoVersions, errChan chan<- error) {
	opt := &github.ListOptions{PerPage: 100, Page: 1}
	allReleases := make([]*semver.Version, 0)
	for {
		releases, res, err := repoService.ListReleases(ctx, repo[0], repo[1], opt)
		if err != nil {
			errChan <- err
			return
		}
		for _, release := range releases {
			verStr, err := sanitizeVersionString(*release.TagName)
			if err != nil {
				continue
			}
			ver, err := semver.NewVersion(verStr)
			if err != nil {
				continue
			}
			allReleases = append(allReleases, ver)
		}
		if res == nil || res.NextPage == 0 {
			break
		}
		opt.Page = res.NextPage
	}
	r := &repoVersions{
		repo:     strings.Join(repo, "/"),
		versions: LatestVersions(allReleases, minVer),
	}
	resultChan <- r
}

func parseFile(ctx context.Context, client GithubClient, file io.Reader) ([]string, error) {
	// read our file line by line
	scanner := bufio.NewScanner(file)
	firstLine := true
	nRepo := 0
	resultChan := make(chan *repoVersions, 32)
	errChan := make(chan error, 32)
	// read line one by one
	// skip first line
	for scanner.Scan() {
		if firstLine {
			firstLine = false
			continue
		}
		line := scanner.Text()
		repo := strings.Split(line, ",")
		if len(repo) != 2 {
			errChan <- fmt.Errorf("Repo malformed, entry provided '%s'", line)
			continue
		}
		v, err := semver.NewVersion(repo[1])
		if err != nil {
			errChan <- fmt.Errorf("Err while parsing version for repo '%s': %s", repo[0], err.Error())
			continue
		}
		repo = strings.Split(repo[0], "/")
		if len(repo) != 2 {
			errChan <- fmt.Errorf("Repo malformed, entry provided '%s'", line)
			continue
		}
		nRepo++
		go getFollowedVersions(ctx, client.Repositories(), repo, v, resultChan, errChan)
	}
	result := make([]string, 0, nRepo)
	var err error
	if nRepo > 0 {
		for {
			select {
			case r := <-resultChan:
				result = append(result, r.String())
				nRepo--
				if nRepo == 0 {
					return result, err
				}
			case err = <-errChan:
				nRepo--
				if nRepo == 0 {
					return result, err
				}
			}
		}
	}
	return result, nil
}
